const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../../middleware/auth');

// Bringing in models
const Post = require('../../models/Post');
//const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @route   POST api/posts
// @desc    Create a Post
// @access  Private
router.post(
  '/',
  [
    auth,
    [
      check('text', 'Please enter some text for the Post')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    try {
      const user = await User.findById(req.user.id).select('-password');

      const newPost = new Post({
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      });

      const post = await newPost.save();

      res.json(post);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error on 34 posts.js');
    }
  }
);

//@route    GET api/posts
//@desc     Get all posts
//@access   Private
router.get('/', auth, async (req, res) => {
  try {
    const allPosts = await Post.find().sort({
      date: -1
    });

    res.json(allPosts);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error');
  }
});

//@route    GET api/posts/:post_id
//@desc     Get a particular post by id
router.get('/:post_id', auth, async (req, res) => {
  try {
    const singlePost = await Post.findById(req.params.post_id);

    if (!singlePost) {
      return res.status(404).json({
        msg: ' Post not found '
      });
    }

    res.json(singlePost);
  } catch (error) {
    console.error(error.message);
    if (error.kind === 'ObjectId') {
      return res.status(404).json({
        msg: ' Post not found ..'
      });
    }
    res.status(500).send('Server Error');
  }
});

// @route   DELETE api/post
// @desc    Delete a user post by it's id
// @access  Private
router.delete('/:post_id', auth, async (req, res) => {
  try {
    const postToBeDeleted = await Post.findById(req.params.post_id);

    if (!postToBeDeleted) {
      return res.status(404).json({
        msg: ' Post not found ..'
      });
    }

    // Check if the user owns the post being deleted
    if (postToBeDeleted.user.toString() !== req.user.id) {
      return res.status(401).json({
        msg: 'Unauthorized User '
      });
    }

    await postToBeDeleted.remove();

    res.json({
      msg: 'Successfully Deleted the Post'
    });
  } catch (error) {
    console.error(error.message);
    if (error.kind === 'ObjectId') {
      return res.status(404).json({
        msg: ' Post not found ...'
      });
    }
    res.status(500).send('Server Error');
  }
});

// @route   PUT api/posts/like/:post_id
// @desc    Like a post
// @access  Private
router.put('/like/:post_id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);

    // Check if the post exists
    if (!post) {
      return res.status(404).json({
        msg: ' Post not found ..'
      });
    }
    // Check if the post has already been liked by this user
    if (
      post.likes.filter(like => like.user.toString() === req.user.id).length > 0
    ) {
      return res.status(400).json({
        msg: 'Post already liked!'
      });
    }

    post.likes.unshift({
      user: req.user.id
    });

    await post.save();

    res.json(post.likes);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error at 137');
  }
});

// @route   PUT api/posts/unlike/:post_id
// @desc    Unlike a Post
// @access  Private
router.put('/unlike/:post_id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);

    // Check if the post exists
    if (!post) {
      return res.status(404).json({
        msg: ' Post not found ..'
      });
    }
    // Check if the post has already been liked by this user
    if (
      post.likes.filter(like => like.user.toString() === req.user.id).length ===
      0
    ) {
      return res.status(400).json({
        msg: 'Post has not yet been liked by you!'
      });
    }

    // Get the index of the like in the like array that needs to be removed
    const indexOfLikeToBeRemoved = post.likes
      .map(like => like.user.toString())
      .indexOf(req.user.id);

    post.likes.splice(indexOfLikeToBeRemoved, 1);

    await post.save();

    res.json(post.likes);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error at 195');
  }
});

// *****Comments*****

// @route   PUT api/posts/comment/:post_id
// @desc    Create a Comment on a Post
// @access  Private
router.put(
  '/comment/:post_id',
  [
    auth,
    [
      check('text', 'Please enter some text for the Post')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    try {
      const user = await User.findById(req.user.id).select('-password');
      const post = await Post.findById(req.params.post_id);

      const newComment = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      };

      post.comments.unshift(newComment);

      await post.save();

      res.json(post.comments);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error on 235 posts.js');
    }
  }
);

// @route   DELETE api/posts/comment/:post_id/:comment_id
// @desc    Delete a comment on a post
// @access  Private
router.delete('/comment/:post_id/:comment_id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);

    // pull out the comment to be deleted
    const commentToBeDeleted = post.comments.find(
      comment => comment.id === req.params.comment_id
    );

    // Check if the comment exists
    if (!commentToBeDeleted) {
      return res.status(404).json({
        msg: 'Comment does not exist'
      });
    }

    // Check if the user deleting the comment is the user that created the comment
    if (commentToBeDeleted.user.toString() !== req.user.id) {
      return res.status(401).json({
        msg: ' Unauthorized user! Pleas login and try again.'
      });
    }

    const commentIndexToBeRemoved = post.comments
      .map(comment => comment.user.toString())
      .indexOf(req.user.id);

    post.comments.splice(commentIndexToBeRemoved, 1);

    await post.save();
    return res.json(post.comments);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error on 248 posts.js');
  }
});

module.exports = router;
