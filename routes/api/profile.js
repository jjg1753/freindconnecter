const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

const Profile = require('../../models/Profile');
const User = require('../../models/User');
const Post = require('../../models/Post');

const { check, validationResult } = require('express-validator/check');

// @route   GET api/profile/me
// @desc    Get current users profile
// @access  Private
router.get('/me', auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.user.id
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);

    if (!profile) {
      return res.status(400).json({
        msg: 'Profile or user not found...'
      });
    }
    res.json(profile);
  } catch (err) {
    console.error(err.message);

    //If statement to check if the object id or the user_id is not a valid mongo db id format
    if (err.kind == 'ObjectId') {
      return res.status(400).json({
        msg: 'Profile or user not found.'
      });
    }
    res.status(500).send('Server Error');
  }
});

// @route   POST api/profile
// @desc    Create or Update usre profile
// @access  Private
router.post(
  '/',
  [
    auth,
    [
      check('status', 'Status is required')
        .not()
        .isEmpty(),
      check('interests', 'Atleast one interest is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    const {
      company,
      website,
      bio,
      status,
      favoritefood,
      privateProfile,
      interests,
      youtube,
      facebook,
      twitter,
      instagram,
      linkedin
    } = req.body;

    // Build a profile model object
    const profilefields = {};
    profilefields.user = req.user.id;

    if (interests) {
      profilefields.interests = interests
        .split(',')
        .map(interest => interest.trim());
    }
    if (privateProfile) profilefields.privateProfile = privateProfile;

    if (company) profilefields.company = company;
    if (website) profilefields.website = website;
    if (bio) profilefields.bio = bio;
    if (status) profilefields.status = status;
    if (favoritefood) profilefields.favoritefood = favoritefood;

    // Build social firelds object
    profilefields.social = {};

    if (youtube) profilefields.social.youtube = youtube;
    if (twitter) profilefields.social.twitter = twitter;
    if (facebook) profilefields.social.facebook = facebook;
    if (instagram) profilefields.social.instagram = instagram;
    if (linkedin) profilefields.social.linkedin = linkedin;

    try {
      let profile = await Profile.findOne({
        user: req.user.id
      });

      if (profile) {
        // Update the existing profile
        profile = await Profile.findOneAndUpdate(
          {
            user: req.user.id
          },
          {
            $set: profilefields
          },
          {
            new: true
          }
        );

        return res.json(profile);
      }

      // If profile is not found create a new profile
      profile = new Profile(profilefields);

      await profile.save();
      res.json(profile);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error occured line 92!');
    }
  }
);

// @route   GET api/profile
// @desc    Get all users profiles
// @access  Public
router.get('/', async (req, res) => {
  try {
    const profiles = await Profile.find({
      privateProfile: false
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);
    res.json(profiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error occured line 122!');
  }
});

// @route   GET api/profile/user/:user_id
// @desc    Get profile by user id
// @access  Private
router.get('/user/:user_id', auth, async (req, res) => {
  try {
    const profiles = await Profile.findOne({
      user: req.params.user_id
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);

    if (!profiles)
      return res.status(400).json({
        msg: 'There is no profile created for this user'
      });

    res.json(profiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error occured line 122!');
  }
});

// @route   DELETE api/profile
// @desc    Delete user and the user profile and posts
// @access  Private
router.delete('/', auth, async (req, res) => {
  try {
    // Remove user posts
    await Post.deleteMany({ user: req.user.id });

    // Remove the profile
    await Profile.findOneAndRemove({
      user: req.user.id
    });

    // Remove the user
    await User.findOneAndRemove({
      _id: req.user.id
    });
    res.json({
      msg: 'User Profile Deleted'
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error occured line 122!');
  }
});

// Using put here because we are updating the profiles object in the database
// by adding an experience in that object and not creating a new object

// @route   PUT api/profile/experience
// @desc    Add experience to the profile
// @access  Private
router.put(
  '/experience',
  [
    auth,
    [
      check('title', 'Title is required')
        .not()
        .isEmpty(),
      check('description', 'Description is required')
        .not()
        .isEmpty(),
      check('from', 'From Date is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    const {
      title,
      withFriends,
      location,
      from,
      to,
      current,
      description
    } = req.body;
    withfriends = withFriends.split(',');

    const newExp = {
      title,
      withfriends,
      location,
      from,
      to,
      current,
      description
    };
    try {
      const profile = await Profile.findOne({
        user: req.user.id
      });

      // unshift method is used to add the new exp to profile array in the beginning
      // so as to make sure that the most recent experience is shown first
      profile.experience.unshift(newExp);

      await profile.save();

      res.json(profile);
    } catch (error) {
      console.error(error.message);
      res.status(500).send('Server Error Occured');
    }
  }
);

// @route   GET api/profile/experience/:exp_id
// @desc    Get experience by experience id
// @access  Private
router.get('/experience/:exp_id', auth, async (req, res) => {
  try {
    const profiles = await Profile.findOne({
      user: req.user.id
    });

    if (!profiles.experience)
      return res.status(400).json({
        msg: 'There is no experience created for this user'
      });

    const selectedExpIndex = profiles.experience
      .map(item => item.id)
      .indexOf(req.params.exp_id);

    res.json(profiles.experience[selectedExpIndex]);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error occured line 122!');
  }
});

// @route   DELETE api/profile/experience/:exp_id
// @desc    Delete experience from profile
// @access  Private
router.delete('/experience/:exp_id', auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.user.id
    });

    // Get the index of experience to be removed
    // Iterate over the experience array and match
    // the index of user to experience id and remove that experience
    const removeIndex = profile.experience
      .map(item => item.id)
      .indexOf(req.params.exp_id);

    // Splice is used to remove the experience
    profile.experience.splice(removeIndex, 1);

    await profile.save();

    res.json(profile);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error Occured!!!');
  }
});

// @route   PUT api/profile/experience/:exp_id
// @desc    Update the experience on the profile
// @access  Private
router.put(
  '/experience/:exp_id',
  [
    auth,
    [
      check('title', 'Title is required')
        .not()
        .isEmpty(),
      check('description', 'Description is required')
        .not()
        .isEmpty(),
      check('from', 'From Date is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }
    const {
      title,
      withFriends,
      location,
      from,
      to,
      current,
      description
    } = req.body;

    if (withFriends) {
      withfriends = withFriends.split(',');
    }

    const updatedExp = {
      title,
      withfriends,
      location,
      from,
      to,
      current,
      description
    };

    try {
      const profile = await Profile.findOne({
        user: req.user.id
      });

      const updateIndex = profile.experience
        .map(item => item.id)
        .indexOf(req.params.exp_id);
      //console.log(updateIndex);

      if (updateIndex !== -1) {
        profile.experience[updateIndex] = updatedExp;
      }

      await profile.save();

      res.json(profile);
    } catch (error) {
      console.error(error.message);
      res.status(500).send('Server Error Occured!!!');
    }
  }
);

// Using put here because we are updating the profiles object in the database
// by adding an education in that object and not creating a new object

// @route   PUT api/profile/education
// @desc    Add education to the profile
// @access  Private
router.put(
  '/education',
  [
    auth,
    [
      check('school', 'School is required')
        .not()
        .isEmpty(),
      check('degree', 'Degree is required')
        .not()
        .isEmpty(),
      check('fieldofstudy', 'Field of study is required')
        .not()
        .isEmpty(),
      check('from', 'From Date is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    const {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description
    } = req.body;

    const newEducation = {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description
    };
    try {
      const profile = await Profile.findOne({
        user: req.user.id
      });

      // unshift method is used to add the new edu to profile array in the beginning
      // so as to make sure that the most recent education is shown first
      profile.education.unshift(newEducation);

      await profile.save();

      res.json(profile);
    } catch (error) {
      console.error(error.message);
      res.status(500).send('Server Error Occured');
    }
  }
);

// @route   GET api/profile/education/:edu_id
// @desc    Get education by education id
// @access  Private
router.get('/education/:edu_id', auth, async (req, res) => {
  try {
    const profiles = await Profile.findOne({
      user: req.user.id
    });

    if (!profiles.education)
      return res.status(400).json({
        msg: 'There is no education created for this user'
      });

    const selectedEduIndex = profiles.education
      .map(item => item.id)
      .indexOf(req.params.edu_id);

    res.json(profiles.education[selectedEduIndex]);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error occured line 433!');
  }
});

// @route   DELETE api/profile/education/:edu_id
// @desc    Delete education from profile
// @access  Private
router.delete('/education/:edu_id', auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.user.id
    });

    // Get the index of education to be removed
    // Iterate over the education array and match
    // the index of user to education id and remove that experience
    const removeIndex = profile.education
      .map(item => item.id)
      .indexOf(req.params.edu_id);

    // Splice is used to remove the education
    profile.education.splice(removeIndex, 1);

    await profile.save();

    res.json(profile);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server Error Occured!!!');
  }
});

// @route   PUT api/profile/education/:edu_id
// @desc    Update the education on the profile
// @access  Private
router.put(
  '/education/:edu_id',
  [
    auth,
    [
      check('school', 'School is required')
        .not()
        .isEmpty(),
      check('degree', 'Degree is required')
        .not()
        .isEmpty(),
      check('fieldofstudy', 'Field of study is required')
        .not()
        .isEmpty(),
      check('from', 'From Date is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }
    const {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description
    } = req.body;

    const updatedEdu = {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description
    };

    try {
      const profile = await Profile.findOne({
        user: req.user.id
      });

      const updateIndex = profile.education
        .map(item => item.id)
        .indexOf(req.params.edu_id);
      //console.log(updateIndex);

      if (updateIndex !== -1) {
        profile.education[updateIndex] = updatedEdu;
      }

      await profile.save();

      res.json(profile);
    } catch (error) {
      console.error(error.message);
      res.status(500).send('Server Error Occured!!!');
    }
  }
);

// ***********************************************************
// The Friendship System APIs

// @route   POST api/profile/sendfriendrequest/:from/:to
// @desc    make a friends request
// @access  Private
// @return  Returns updated friendrequests of from
router.post('/sendfriendrequest/:from/:to', auth, async (req, res) => {
  try {
    const profile_from = await Profile.findOne({
      user: req.params.from
    });

    const profile_to = await Profile.findOne({
      user: req.params.to
    });

    if (!profile_from || !profile_to) {
      return res.status(404).json({
        errors: [
          {
            msg:
              "Either of the user accounts don't exist. Please try refreshing the page."
          }
        ]
      });
    }

    if (
      profile_from.sentfriendrequests &&
      profile_from.sentfriendrequests.includes(req.params.to)
    ) {
      return res.status(400).json({
        errors: [
          {
            msg: 'Friend Request already sent.'
          }
        ]
      });
    }

    if (
      profile_to.receivedfriendrequests &&
      profile_to.receivedfriendrequests.includes(req.params.from)
    ) {
      return res.status(400).json({
        errors: [
          {
            msg: 'Friend Request already sent.'
          }
        ]
      });
    }

    // res.json(profile_from)
    if (profile_from.friends && !profile_from.friends.includes(req.params.to)) {
      // profile_from.friends.unshift(req.params.to);
      profile_from.sentfriendrequests.unshift(req.params.to);
      await profile_from.save();
    } else if (!profile_from.friends) {
      // profile_from.friends.push(req.params.to);
      profile_from.sentfriendrequests.push(req.params.to);
      await profile_from.save();
    }

    if (profile_to.friends && !profile_to.friends.includes(req.params.from)) {
      // profile_to.friends.unshift(req.params.from);
      profile_to.receivedfriendrequests.unshift(req.params.from);
      await profile_to.save();
    } else if (!profile_to.friends) {
      // profile_to.friends.push(req.params.from);
      profile_to.receivedfriendrequests.push(req.params.from);
      await profile_to.save();
    }

    const profile = await Profile.findOne({
      user: req.params.from
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);
    // console.log(JSON.stringify(profile))

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error on 674 profile.js');
  }
});

// @route   POST api/profile/acceptfriendrequest/:from/:to
// @desc    Accept a friends request
// @access  Private
// @return  Returns updated friendrequests of to
router.post('/acceptfriendrequest/:from/:to', auth, async (req, res) => {
  try {
    const profile_from = await Profile.findOne({
      user: req.params.from
    });

    const profile_to = await Profile.findOne({
      user: req.params.to
    });

    if (!profile_from || !profile_to) {
      return res.status(404).json({
        errors: [
          {
            msg:
              "Either of the user accounts don't exist. Please try refreshing the page."
          }
        ]
      });
    }

    // Profile_from friends
    if (profile_from.friends && !profile_from.friends.includes(req.params.to)) {
      profile_from.friends.unshift(req.params.to);
    } else if (!profile_from.friends) {
      profile_from.friends.push(req.params.to);
    }

    // Profile_to friends
    if (profile_to.friends && !profile_to.friends.includes(req.params.from)) {
      profile_to.friends.unshift(req.params.from);
    } else if (!profile_to.friends) {
      profile_to.friends.push(req.params.from);
    }

    // Profile_from sentfriendrequests
    if (
      profile_from.sentfriendrequests &&
      profile_from.sentfriendrequests.includes(req.params.to)
    ) {
      profile_from.sentfriendrequests.splice(
        profile_from.sentfriendrequests.findIndex(sfr => sfr === req.params.to),
        1
      );

      /*profile_from.sentfriendrequests = profile_from.sentfriendrequests.filter(
        prf => prf !== req.params.to
      );*/
    }

    //profile_to receivedfriendrequests
    if (
      profile_to.receivedfriendrequests &&
      profile_to.receivedfriendrequests.includes(req.params.from)
    ) {
      profile_to.receivedfriendrequests.splice(
        profile_to.receivedfriendrequests.findIndex(
          sfr => sfr === req.params.from
        ),
        1
      );

      /*profile_to.receivedfriendrequests = profile_to.receivedfriendrequests.filter(
        rcf => rcf !== req.params.from
      );*/
    }

    // console.log(profile_to);
    // console.log('----------------');
    // console.log(profile_from);

    await profile_to.save();
    await profile_from.save();

    const profile = await Profile.findOne({
      user: req.params.to
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);
    // console.log(JSON.stringify(profile))

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error on 847 profile.js');
  }
});

// @route   POST api/profile/rejectfriendrequest/:from/:to
// @desc    Reject a friend request
// @access  Private
// @return  Returns updated friendrequests of to
router.post('/rejectfriendrequest/:from/:to', auth, async (req, res) => {
  try {
    const profile_from = await Profile.findOne({
      user: req.params.from
    });

    const profile_to = await Profile.findOne({
      user: req.params.to
    });

    if (!profile_from || !profile_to) {
      return res.status(404).json({
        errors: [
          {
            msg:
              "Either of the user accounts don't exist. Please try refreshing the page."
          }
        ]
      });
    }

    // Profile_from friends
    if (profile_from.friends && profile_from.friends.includes(req.params.to)) {
      profile_from.friends.splice(
        profile_from.friends.findIndex(frs => frs === req.params.to),
        1
      );
    }

    // Profile_to friends
    if (profile_to.friends && profile_to.friends.includes(req.params.from)) {
      profile_to.friends.splice(
        profile_to.friends.findIndex(frs => frs === req.params.from),
        1
      );
    }

    // Profile_from sentfriendrequests
    if (
      profile_from.sentfriendrequests &&
      profile_from.sentfriendrequests.includes(req.params.to)
    ) {
      profile_from.sentfriendrequests.splice(
        profile_from.sentfriendrequests.findIndex(sfr => sfr === req.params.to),
        1
      );

      /*profile_from.sentfriendrequests = profile_from.sentfriendrequests.filter(
        prf => prf !== req.params.to
      );*/
    }

    //profile_to receivedfriendrequests
    if (
      profile_to.receivedfriendrequests &&
      profile_to.receivedfriendrequests.includes(req.params.from)
    ) {
      profile_to.receivedfriendrequests.splice(
        profile_to.receivedfriendrequests.findIndex(
          sfr => sfr === req.params.from
        ),
        1
      );
    }
    // console.log(profile_to);
    // console.log('----------------');
    // console.log(profile_from);

    await profile_to.save();
    await profile_from.save();

    const profile = await Profile.findOne({
      user: req.params.to
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);
    // console.log(JSON.stringify(profile))

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error on 847 profile.js');
  }
});

// @route   POST api/profile/removefriend/:from/:to
// @desc    Unfriend
// @access  Private
// @return  Returns updated friendrequests of from
router.post('/removefriend/:from/:to', auth, async (req, res) => {
  try {
    const profile_from = await Profile.findOne({
      user: req.params.from
    });

    const profile_to = await Profile.findOne({
      user: req.params.to
    });
    // console.log(profile_from);

    if (!profile_from || !profile_to) {
      return res.status(404).json({
        errors: [
          {
            msg:
              "Either of the user accounts don't exist. Please try refreshing the page."
          }
        ]
      });
    }

    // Profile_from friends
    if (profile_from.friends && profile_from.friends.includes(req.params.to)) {
      profile_from.friends.splice(
        profile_from.friends.findIndex(frs => frs === req.params.to),
        1
      );
    }

    // Profile_to friends
    if (profile_to.friends && profile_to.friends.includes(req.params.from)) {
      profile_to.friends.splice(
        profile_to.friends.findIndex(frs => frs === req.params.from),
        1
      );
    }

    // Profile_from sentfriendrequests
    profile_from.sentfriendrequests &&
      profile_from.sentfriendrequests.includes(req.params.to) &&
      profile_from.sentfriendrequests.splice(
        profile_from.sentfriendrequests.findIndex(sfr => sfr === req.params.to),
        1
      );
    // Profile_to sentfriendrequests
    profile_to.sentfriendrequests &&
      profile_to.sentfriendrequests.includes(req.params.from) &&
      profile_to.sentfriendrequests.splice(
        profile_to.sentfriendrequests.findIndex(sfr => sfr === req.params.from),
        1
      );

    //profile_to receivedfriendrequests
    profile_to.receivedfriendrequests &&
      profile_to.receivedfriendrequests.includes(req.params.from) &&
      profile_to.receivedfriendrequests.splice(
        profile_to.receivedfriendrequests.findIndex(
          sfr => sfr === req.params.from
        ),
        1
      );

    // profile_from receivedfriendrequests
    profile_from.receivedfriendrequests &&
      profile_from.receivedfriendrequests.includes(req.params.to) &&
      profile_from.receivedfriendrequests.splice(
        profile_from.receivedfriendrequests.findIndex(
          sfr => sfr === req.params.to
        ),
        1
      );
    // console.log(profile_to);
    // console.log('----------------');
    // console.log(profile_from);

    await profile_to.save();
    await profile_from.save();
    // console.log(JSON.stringify(profile_to));

    const profile = await Profile.findOne({
      user: req.params.from
    })
      .populate('user', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('friends', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('sentfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ])
      .populate('receivedfriendrequests', [
        'name',
        'avatar',
        'location',
        'summary',
        'date_of_birth'
      ]);
    // console.log(JSON.stringify(profile))

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error on 847 profile.js');
  }
});

// @route   POST api/profile/removefriend/:from/:to
// @desc    Unfriend
// @access  Private
// @return  Returns updated friendrequests of from
/*router.post('/reset', auth, async (req, res) => {
  const i = 0;
  Profile.find.map(function(err, profile) {
    profile = profile[i];
    profile.friends && profile.friends.splice(0, profile.friends.length);
    profile.sentfriendrequests &&
      profile.sentfriendrequests.splice(0, profile.sentfriendrequests.length);
    profile.receivedfriendrequests &&
      profile.receivedfriendrequests.splice(
        0,
        profile.receivedfriendrequests.lenght
      );
    console.log(profile);

    profile.save(function(err) {
      if (err) {
        console.error('ERROR!');
      }
    });
  });

  //await allprofiles.save();
});*/

module.exports = router;
