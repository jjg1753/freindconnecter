const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

const { check, validationResult } = require('express-validator/check');

const User = require('../../models/User');

// @route   POST api/users
// @desc    Register a new user
// @access  Public

let d = new Date();
// let cB = new Date(year, month, day).toDateString();

router.post(
  '/',
  [
    check('name', 'Name is required')
      .not()
      .isEmpty(),
    check('email', 'Please include a valid email ID').isEmail(),
    check(
      'password',
      'Please enter a password which is at least 6 character long'
    ).isLength({
      min: 6
    })
    //check('dob', 'Invalid date of birth').isBefore(cB)
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    // Pull out the details from the request object
    const { name, email, password, dob, summary, location } = req.body;

    try {
      // It is basically email: email but since it is the same we can just write email in the findOne method
      let user = await User.findOne({
        email
      });
      // Check if the user already exists to avoid duplicate accounts

      if (user) {
        return res.status(400).json({
          errors: [
            {
              msg:
                'User with the same email address already exists. Try logging in with that account or create a new account. '
            }
          ]
        });
      }

      // Get users gravatar
      const avatar = gravatar.url(email, {
        s: '200',
        r: 'pg',
        d: 'mm'
      });

      // Replace the user res contents with a new User model object
      user = new User({
        name,
        email,
        avatar,
        password,
        dob,
        summary,
        location
      });

      // Password Encryption using bcrypt
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      // Insert user in the database and it gives a promise in user
      await user.save();

      // Return JSONwebtoken to log the user in
      // user.id in here is ._id of mongodb
      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        {
          expiresIn: 36000
        },
        (err, token) => {
          if (err) throw err;
          res.json({
            token
          });
        }
      );
    } catch {
      console.error(err.message);
      res.status(500).send('Some Server Error');
    }
  }
);

module.exports = router;
