const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const User = require('../../models/User');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcryptjs');


const {
    check,
    validationResult
} = require('express-validator/check');

// @route   GET api/auth
// @desc    Authentication Route
// @access  Public
router.get('/', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    } catch {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


// @route   POST api/auth
// @desc    Authenticate an existing user and get token
// @access  Public

router.post('/', [
    check('email', 'Please include a valid email ID').isEmail(),
    check('password', 'Please enter a password').exists()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    // Pull out the details from the request object
    const {
        email,
        password
    } = req.body;

    try {
        // It is basically email: email but since it is the same we can just write email in the findOne method
        let user = await User.findOne({
            email
        });
        // Check if the user account exists

        if (!user) {
            return res.status(400).json({
                errors: [{
                    msg: 'Email and/or Password do not match and/or do not exist. Access Denied.'
                }]
            });
        }

        // Check if the password matches using compare method of bcrypt
        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(400).json({
                errors: [{
                    msg: 'Email and/or Password do not match and/or do not exist. Access Denied.'
                }]
            });
        }

        // Return JSONwebtoken to log the user in
        // user.id in here is ._id of mongodb
        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(payload, config.get('jwtSecret'), {
            expiresIn: 360000
        }, (err, token) => {
            if (err) throw err;
            res.json({
                token
            });
        });

    } catch {
        console.error(err.message);
        res.status(500).send("Some Server Error");
    }

});


module.exports = router;