/* this is a middleware function that has access to req and res 
   object and authenticates the token and moves on to next middleware */

const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    // Retrieving token from the header

    const token = req.header('x-auth-token');

    // Check if there is no token
    if (!token) {
        return res.status(401).json({
            msg: 'Verification token absent. Access denied...'
        });
    }

    // Verify the token
    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));

        // user has the id from the payload and we can use the user in any of the protected routes
        req.user = decoded.user;
        next();
    } catch (err) {
        res.status(401).json({
            msg: 'Token authentication failed...'
        });
    }
}