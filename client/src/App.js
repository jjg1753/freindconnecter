import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Routes from './components/routing/Routes';
import setAuthToken from './helper/setAuthToken';
import { loadUser } from './actions/auth';

// For Redux
import { Provider } from 'react-redux';
import store from './store';

import './App.css';

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  // Use effect is a hook which acts like ComponentDidMount
  // Since we are not using classes and winstead we are functions
  // lifecycle methods are implemented using hooks.
  // The empty array [] argument in useEffect tells it to
  // execute dispatch only once and not keep on doing it in a loop
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Landing} />
            <Route component={Routes} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
