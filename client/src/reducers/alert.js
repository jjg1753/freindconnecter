import { SET_ALERT, REMOVE_ALERT } from '../actions/constants';

const initialState = [];

export default function(state = initialState, action) {
  // Destructuring the action object and extracting type and payload from that
  const { type, payload } = action;

  switch (type) {
    case SET_ALERT:
      // ...state is because state is immutable so we
      // we are just adding our payload to the already existing
      // state and not changing anything in that
      return [...state, payload];

    case REMOVE_ALERT:
      // Remove specific alert by id
      return state.filter(alert => alert.id !== payload);
    default:
      return state;
  }
}
