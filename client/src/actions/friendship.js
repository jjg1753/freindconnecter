import axios from 'axios';
import { setAlert } from './alert';

import {
  SEND_FRIEND_REQUEST,
  FRIENDSHIP_ERROR,
  ACCEPT_FRIEND_REQUEST,
  REJECT_FRIEND_REQUEST,
  UNFRIEND
} from './constants';

// Make a new friend
export const sendfriendrequest = (from, to) => async dispatch => {
  try {
    const res = await axios.post(
      `/api/profile/sendfriendrequest/${from}/${to}`
    );
    dispatch({
      type: SEND_FRIEND_REQUEST,
      payload: res.data
    });
    dispatch(setAlert('Friend Request Sent!', 'success'));
    //history.push('/profiles')
    // dispatch(loadUser());
  } catch (err) {
    //console.error(err);
    dispatch(
      setAlert(
        'Some problem occured. Please refresh the page and try again!',
        'danger'
      )
    );
    dispatch({
      type: FRIENDSHIP_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Accept a friend request
export const acceptfriendrequest = (from, to) => async dispatch => {
  try {
    const res = await axios.post(
      `/api/profile/acceptfriendrequest/${from}/${to}`
    );
    dispatch({
      type: ACCEPT_FRIEND_REQUEST,
      payload: res.data
    });
    dispatch(setAlert('Friend Request Accepted!', 'success'));
    //history.push('/profiles')
    // dispatch(loadUser());
  } catch (err) {
    //console.error(err);
    dispatch(
      setAlert('Server Error Occured. Please Refresh And Try Again', 'danger')
    );
    dispatch({
      type: FRIENDSHIP_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

// Reject a friend request
export const rejectfriendrequest = (from, to) => async dispatch => {
    try {
      const res = await axios.post(
        `/api/profile/rejectfriendrequest/${from}/${to}`
      );
      dispatch({
        type: REJECT_FRIEND_REQUEST,
        payload: res.data
      });
      dispatch(setAlert('Friend Request Rejcted!', 'danger'));
      //history.push('/profiles')
      // dispatch(loadUser());
    } catch (err) {
      //console.error(err);
      dispatch(
        setAlert('Server Error Occured. Please Refresh And Try Again', 'danger')
      );
      dispatch({
        type: FRIENDSHIP_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  };


  // Unfriended
export const unfriend = (from, to) => async dispatch => {
    try {
      const res = await axios.post(
        `/api/profile/removefriend/${from}/${to}`
      );
      dispatch({
        type: UNFRIEND,
        payload: res.data
      });
      dispatch(setAlert('Unfriended !', 'danger'));
    } catch (err) {
      //console.error(err);
      dispatch(
        setAlert('Server Error Occured. Please Refresh And Try Again', 'danger')
      );
      dispatch({
        type: FRIENDSHIP_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  };