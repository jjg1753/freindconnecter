import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getCurrentProfile } from '../../actions/profile';
import { deleteEducation } from '../../actions/education';

const DisplayEducation = ({
  getCurrentProfile,
  profile: {
    profile: { education }
  },
  deleteEducation
}) => {
  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);
  return (
    <Fragment>
      <h2 className='my-2'>Education Credentials</h2>
      <table className='table'>
        <thead>
          <tr>
            <th>School</th>
            <th className='hide-sm'>Degree</th>
            <th className='hide-sm'>Field Of Study</th>
            <th className='hide-sm'>Years</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {education.map((edu, index) => {
            return (
              <tr key={index}>
                <td>{edu.school}</td>
                <td className='hide-sm'>{edu.degree}</td>
                <td className='hide-sm'>{edu.fieldofstudy}</td>
                <td className='hide-sm'>
                  {new Date(edu.from.toString()).getMonth() +
                    1 +
                    '/' +
                    new Date(edu.from.toString()).getFullYear()}{' '}
                  -{' '}
                  {edu.to
                    ? new Date(edu.to.toString()).getMonth() +
                      1 +
                      '/' +
                      new Date(edu.to.toString()).getFullYear()
                    : 'now'}
                </td>
                <td>
                  <button
                    onClick={() => deleteEducation(edu._id)}
                    className='btn btn-danger'
                  >
                    <i className='fas fa-times' />
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </Fragment>
  );
};

DisplayEducation.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  deleteEducation: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, deleteEducation }
)(DisplayEducation);
