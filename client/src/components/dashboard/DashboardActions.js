import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const DashboardActions = ({ auth: { user } }) => {
  return (
    <div className='dash-buttons'>
      <Link to='/experience' className='btn btn-light'>
        <i className='fab fa-black-tie text-primary' /> Add Experience
      </Link>
      <Link to='/education' className='btn btn-light'>
        <i className='fas fa-graduation-cap text-primary' /> Add Education
      </Link>
      <Link to={`/profile/${user._id}`} className='btn btn-light'>
        <i className='fas fa-user text-primary' /> My Profile
      </Link>
      <Link to='/edit-profile' className='btn btn-light'>
        <i className='fas fa-edit text-primary' /> Edit Profile
      </Link>
    </div>
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  {}
)(DashboardActions);
