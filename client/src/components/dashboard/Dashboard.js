import React, { useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentProfile, deleteAccount } from '../../actions/profile';
import Spinner from '../layout/Spinner';
import DashboardActions from './DashboardActions';
import DisplayEducation from './DisplayEducation';
import DisplayExperience from './DisplayExperience';

const Dashboard = ({
  getCurrentProfile,
  auth: { user },
  profile: { profile, loading },
  deleteAccount
}) => {
  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);

  return loading && profile == null ? (
    <Spinner />
  ) : (
    <Fragment>
      <h1 className='large text-primary'>Dashboard</h1>
      <p className='lead'>
        <i className='fas fa-user' /> Welcome {user && user.name} !
      </p>
      {profile != null ? (
        <Fragment>
          <DashboardActions />

          {profile.education.length > 0 ? (
            <DisplayEducation />
          ) : (
            <p>
              You don't have any education added to your profile. Click{' '}
              <Link to='/education'> here </Link> to add one!
            </p>
          )}
          {profile.experience.length > 0 ? (
            <DisplayExperience />
          ) : (
            <p>
              You don't have any experience added to your profile. Click{' '}
              <Link to='/experience'> here </Link> to add one!
            </p>
          )}
          <div className='my-2'>
            <button className='btn btn-danger' onClick={() => deleteAccount()}>
              <i className='fas fa-user-minus' />
              Delete My Account
            </button>
          </div>
        </Fragment>
      ) : (
        <Fragment>
          <p>
            {' '}
            You don't have a profile set up yet. Please go ahead and create a
            new one!
          </p>
          <Link to='/create-profile' className='btn btn-primary my-1'>
            {' '}
            Create Profile{' '}
          </Link>
        </Fragment>
      )}
    </Fragment>
  );
};

Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  deleteAccount: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, deleteAccount }
)(Dashboard);
