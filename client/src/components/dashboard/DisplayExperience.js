import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getCurrentProfile } from '../../actions/profile';
import { deleteExperience } from '../../actions/experience';

const DisplayExperience = ({
  getCurrentProfile,
  profile: {
    profile: { experience }
  },
  deleteExperience
}) => {
  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);
  return (
    <Fragment>
      <h2 className='my-2'>Life Experiences</h2>
      <table className='table'>
        <thead>
          <tr>
            <th>Title</th>
            <th className='hide-sm'>Location</th>
            <th className='hide-sm'>Duration</th>
            <th className='hide-sm'>Tagged Friends</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {experience.map((exp, index) => {
            return (
              <tr key={index}>
                <td>{exp.title}</td>
                <td className='hide-sm'>{exp.location}</td>
                <td className='hide-sm'>
                  {new Date(exp.from.toString()).getMonth() +
                    1 +
                    '/' +
                    new Date(exp.from.toString()).getFullYear()}{' '}
                  -{' '}
                  {exp.to
                    ? new Date(exp.to.toString()).getMonth() +
                      1 +
                      '/' +
                      new Date(exp.to.toString()).getFullYear()
                    : 'now'}
                </td>
                <td className='hide-sm'>{exp.withfriends.join(', ')}</td>
                <td>
                  <button
                    onClick={() => deleteExperience(exp._id)}
                    className='btn btn-danger'
                  >
                    <i className='fas fa-times' />
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </Fragment>
  );
};

DisplayExperience.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  deleteExperience: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, deleteExperience }
)(DisplayExperience);
