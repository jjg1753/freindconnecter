import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createExperience } from '../../actions/experience';
import { Link, withRouter } from 'react-router-dom';

const CreateExperience = ({ createExperience, history }) => {
  const [formData, setFormData] = useState({
    title: '',
    withFriends: '',
    location: '',
    from: '',
    current: false,
    to: '',
    description: ''
  });

  const [displayToDate, toggleToDate] = useState(true);

  const {
    title,
    withFriends,
    location,
    from,
    to,
    current,
    description
  } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    createExperience(formData, history);
  };

  const onCheckChange = e => {
    toggleToDate(!displayToDate);
    setFormData({ ...formData, current: !current, to: '' });
  };

  return (
    <Fragment>
      <h1 className='large text-primary'>Share a Life Experience</h1>
      <p className='lead'>
        <i className='far fa-life-ring' /> Share one of your epic life experiences
      </p>
      <small>* = required field</small>
      <form className='form' onSubmit={e => onSubmit(e)}>
        <div className='form-group'>
          <input type='text' placeholder='* Title' name='title' required value={title}
            onChange={e => onChange(e)}/>
        </div>
        <div className='form-group'>
          <input type='text' placeholder='Tag any friends that were a part of this experience' name='withFriends' value={withFriends}
            onChange={e => onChange(e)}/>
            <small className='form-text'>
            Please use comma separated values (eg.
            Raj,Yash,Hardik,Aman,Anmol )
          </small>
        </div>
        <div className='form-group'>
          <input type='text' placeholder='Location' name='location' value={location}
            onChange={e => onChange(e)}/>
        </div>
        <div className='form-group'>
          <h4>From Date</h4>
          <input type='date' name='from' value={from}
            onChange={e => onChange(e)}/>
        </div>
        <div className='form-group'>
          <p>
            <input type='checkbox' name='current' value={current}
              onChange={e => onCheckChange(e)} /> Currently Experiencing
          </p>
        </div>
        {displayToDate && (
          <Fragment>
            <div className='form-group'>
              <h4>To Date</h4>
              <input
                type='date'
                name='to'
                value={to}
                onChange={e => onChange(e)}
              />
            </div>
          </Fragment>
        )}

        <div className='form-group'>
          <textarea
            name='description'
            cols='30'
            rows='5'
            placeholder='How did this experience effect/change your life?'
            value={description}
            onChange={e => onChange(e)}
          />
        </div>
        <input type='submit' className='btn btn-primary my-1' />
        <Link to='/dashboard' className='btn btn-light my-1'>
          Go Back
        </Link>
      </form>
    </Fragment>
  );
};

CreateExperience.propTypes = {
  createExperience: PropTypes.func.isRequired
};

export default connect(
  null,
  { createExperience }
)(withRouter(CreateExperience));
