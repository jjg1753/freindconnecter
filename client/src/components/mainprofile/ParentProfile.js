import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import { getProfileById } from '../../actions/profile';
import { Link } from 'react-router-dom';
import ProfileTop from './ProfileTop';
import ProfileAbout from './ProfileAbout';
import ProfileExperience from './ProfileExperience';
import ProfileEducation from './ProfileEducation';
import { getPosts } from '../../actions/post';
import {
  sendfriendrequest,
  acceptfriendrequest,
  rejectfriendrequest,
  unfriend
} from '../../actions/friendship';
import PostItem from '../posts/PostItem';

const onAccept = (from, to, acceptfriendrequest) => {
  acceptfriendrequest(from, to);
  window.location.reload(true);
};

const onUnfriend = (from, to, unfriend) => {
  unfriend(from, to);
  window.location.reload(true);
};

const onRejectfriendrequest = (from, to, rejectfriendrequest) => {
  rejectfriendrequest(from, to);
  window.location.reload(true);
};

const onSendfriendrequest = (from, to, sendfriendrequest) => {
  sendfriendrequest(from, to);
  window.location.reload(true);
};

const onWithdraw = (from, to, withdrawfriendrequest) => {
  withdrawfriendrequest(from, to);
  window.location.reload(true);
};

const ParentProfile = ({
  getProfileById,
  profile: { profile, loading },
  auth,
  match,
  unfriend,
  sendfriendrequest,
  rejectfriendrequest,
  acceptfriendrequest,
  getPosts,
  post: { posts }
}) => {
  useEffect(() => {
    getPosts();
    getProfileById(match.params.id);
  }, [getProfileById, match.params.id, getPosts]);
  return (
    <Fragment>
      {profile === null || loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Link to='/profiles' className='btn btn-light'>
            Back to Profiles
          </Link>
          {auth.user._id !== profile.user._id && (
            <Fragment>
              {profile.sentfriendrequests.some(e => e._id === auth.user._id) ? (
                <Fragment>
                  <button
                    onClick={() =>
                      onAccept(
                        profile.user._id,
                        auth.user._id,
                        acceptfriendrequest
                      )
                    }
                    className='btn btn-success'
                  >
                    <i className='fas fa-user-plus' />{' '}
                    <span className='hide-sm'>Accept</span>{' '}
                  </button>
                  <button
                    onClick={() =>
                      onRejectfriendrequest(
                        profile.user._id,
                        auth.user._id,
                        rejectfriendrequest
                      )
                    }
                    className='btn btn-danger'
                  >
                    <i className='fas fa-user-minus' />{' '}
                    <span className='hide-sm'>Reject</span>{' '}
                  </button>
                </Fragment>
              ) : (
                <Fragment>
                  {profile.receivedfriendrequests.some(
                    e => e._id === auth.user._id
                  ) ? (
                    <button
                      onClick={() =>
                        onWithdraw(
                          auth.user._id,
                          profile._id,
                          rejectfriendrequest
                        )
                      }
                      className='btn btn-danger'
                    >
                      <i className='fas fa-user-minus' />{' '}
                      <span className='hide-sm'>Withdraw friend request</span>{' '}
                    </button>
                  ) : (
                    <Fragment>
                      {profile.friends.filter(e => e._id === auth.user._id)
                        .length > 0 ? (
                        <button
                          onClick={() =>
                            onUnfriend(auth.user._id, profile._id, unfriend)
                          }
                          className='btn btn-danger'
                        >
                          <i className='fas fa-user-minus' />{' '}
                          <span className='hide-sm'>Unfriend</span>{' '}
                        </button>
                      ) : (
                        <button
                          onClick={() =>
                            onSendfriendrequest(
                              auth.user._id,
                              profile.user._id,
                              sendfriendrequest
                            )
                          }
                          className='btn btn-outline-success'
                        >
                          <i className='fas fa-user-plus' />{' '}
                          <span className='hide-sm'>Add Friend</span>{' '}
                        </button>
                      )}{' '}
                    </Fragment>
                  )}
                </Fragment>
              )}
            </Fragment>
          )}
          {auth.isAuthenticated &&
            auth.loading === false &&
            auth.user._id === profile.user._id && (
              <Link to='/edit-profile' className='btn btn-dark'>
                Edit Profile
              </Link>
            )}
          <Fragment>
            <div className='profile-grid my-1'>
              <ProfileTop profile={profile} />
              {profile.friends.filter(friend => friend._id === auth.user._id)
                .length > 0 || profile.user._id === auth.user._id ? (
                <Fragment>
                  <ProfileAbout profile={profile} />
                  <div className='profile-exp bg-white p-2'>
                    <h2 className='text-primary'>Life Experiences</h2>
                    {profile.experience.length > 0 ? (
                      <Fragment>
                        {profile.experience.map(exp => (
                          <ProfileExperience key={exp._id} experience={exp} />
                        ))}
                      </Fragment>
                    ) : (
                      <h4>No Experiences added yet.</h4>
                    )}
                  </div>
                  <div className='profile-edu bg-white p-2'>
                    <h2 className='text-primary'>Education</h2>
                    {profile.education.length > 0 ? (
                      <Fragment>
                        {profile.education.map(edu => (
                          <ProfileEducation key={edu._id} education={edu} />
                        ))}
                      </Fragment>
                    ) : (
                      <h4>No Educations added yet.</h4>
                    )}
                  </div>
                  {posts.filter(post => post.user === profile.user._id).length >
                    0 && (
                    <Fragment>
                      <div>
                        <p className='lead'>Posts by {profile.user.name}</p>
                        <div className='posts'>
                          {posts.map(
                            post =>
                              post.user === profile.user._id && (
                                <PostItem key={post._id} post={post} />
                              )
                          )}
                        </div>
                      </div>
                    </Fragment>
                  )}
                </Fragment>
              ) : (
                <Fragment>
                  {' '}
                  Please add {profile.user.name} as your friend to view full
                  profile.{' '}
                  <button
                    onClick={() =>
                      onSendfriendrequest(
                        auth.user._id,
                        profile.user._id,
                        sendfriendrequest
                      )
                    }
                    className='btn btn-success'
                  >
                    <i className='fas fa-user-plus' />{' '}
                    <span className='hide-sm'>Add Friend</span>{' '}
                  </button>
                </Fragment>
              )}
            </div>
          </Fragment>
        </Fragment>
      )}
    </Fragment>
  );
};

ParentProfile.propTypes = {
  getProfileById: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  sendfriendrequest: PropTypes.func.isRequired,
  acceptfriendrequest: PropTypes.func.isRequired,
  rejectfriendrequest: PropTypes.func.isRequired,
  unfriend: PropTypes.func.isRequired,
  getPosts: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth,
  post: state.post
});

export default connect(
  mapStateToProps,
  {
    getProfileById,
    unfriend,
    rejectfriendrequest,
    acceptfriendrequest,
    sendfriendrequest,
    getPosts
  }
)(ParentProfile);
