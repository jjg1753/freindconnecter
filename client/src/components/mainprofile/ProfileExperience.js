import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

const ProfileExperience = ({
  experience: { title, withfriends, location, from, to, current, description }
}) => (
  <div>
    <h3 className='text-dark'>{title}</h3>
    <p>
      <Moment format='YYYY/MM/DD'>{from}</Moment> -{' '}
      {!to ? 'Now' : <Moment format='YYYY/MM/DD'>{to}</Moment>}
    </p>
    <p>
      <strong>Location: </strong> {location}
    </p>
    <p>
      <strong>Description: </strong> {description}
    </p>
    <strong>Tagged Friends</strong>
    {withfriends && (
      <ul>
        {withfriends.map((friend, index) => (
          <li key={index}>
            <i className='fas fa-tag' /> {friend}
          </li>
        ))}
      </ul>
    )}
  </div>
);

ProfileExperience.propTypes = {
  experience: PropTypes.object.isRequired
};

export default ProfileExperience;
