import React, { Fragment, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';

// Connect is used to connect a component to redux and also connecting actions to components
import { connect } from 'react-redux';
import { setAlert } from '../../actions/alert';
import { register } from '../../actions/auth';
import PropTypes from 'prop-types';

const Register = ({ setAlert, register, isAuthenticated }) => {
  //This line is basically setting the default state for the component
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    confirmPass: '',
    dob: '',
    summary: '',
    location: ''
  });

  const {
    name,
    email,
    password,
    confirmPass,
    dob,
    summary,
    location
  } = formData;

  // On change function used for any of the fields in the form
  // [func.target.name] takes the "name" value of the form component and changes that in the state
  const onChange = args =>
    setFormData({ ...formData, [args.target.name]: args.target.value });

  const onSubmit = async args => {
    args.preventDefault();
    if (password !== confirmPass) {
      setAlert("Passwords don't match. Please try Again!", 'danger');
    } else {
      //   const newUser = {
      //     name,
      //     email,
      //     password,
      //     dob,
      //     summary,
      //     location
      //   };

      //   try {
      //     const config = {
      //       headers: {
      //         'Content-Type': 'application/json'
      //       }
      //     };

      //     const body = JSON.stringify(newUser);

      //     //axios returns a promise
      //     // ./ because we have already set up a proxy for the front-end
      //     const res = await axios.post('./api/users', body, config);
      //     console.log(res.data);
      //   } catch (error) {
      //     console.error(error.response.data);
      //   }

      register({ name, email, password, dob, summary, location });
    }
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <Fragment>
      <h1 className='large text-primary'>Sign Up</h1>
      <p className='lead'>
        <i className='fas fa-user' /> Create Your Account
      </p>
      <form className='form' onSubmit={args => onSubmit(args)}>
        <div className='form-group'>
          <input
            type='text'
            placeholder='Name'
            name='name'
            value={name}
            onChange={args => onChange(args)}
            //required
          />
        </div>
        <div className='form-group'>
          <input
            type='email'
            placeholder='Email Address'
            name='email'
            value={email}
            onChange={args => onChange(args)}
            required
          />
          <small className='form-text'>
            This site uses Gravatar so if you want a profile image, use a
            Gravatar email
          </small>
        </div>
        <div className='form-group'>
          <input
            type='password'
            placeholder='Password'
            name='password'
            minLength='6'
            value={password}
            onChange={args => onChange(args)}
            required
          />
        </div>
        <div className='form-group'>
          <input
            type='password'
            placeholder='Confirm Password'
            name='confirmPass'
            minLength='6'
            value={confirmPass}
            onChange={args => onChange(args)}
          />
        </div>
        <div className='form-group'>
          <h4>Enter your Birth Date:</h4>
          <input
            type='date'
            name='dob'
            value={dob}
            onChange={args => onChange(args)}
          />
        </div>
        <div className='form-group'>
          <input
            type='text'
            placeholder='Tell us something about yourself'
            name='summary'
            value={summary}
            onChange={args => onChange(args)}
            required
          />
        </div>
        <div className='form-group'>
          <input
            type='text'
            placeholder='Enter your city'
            name='location'
            value={location}
            onChange={args => onChange(args)}
            required
          />
        </div>
        <input type='submit' className='btn btn-primary' value='Register' />
      </form>
      <p className='my-1'>
        Already have an account? <Link to='/login'>Sign In</Link>
      </p>
    </Fragment>
  );
};

Register.propTypes = {
  setAlert: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

// This line allows us to use props.setAlert
export default connect(
  mapStateToProps,
  { setAlert, register }
)(Register);
