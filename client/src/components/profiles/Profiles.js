import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getProfiles, getCurrentProfile } from '../../actions/profile';
import Spinner from '../layout/Spinner';
import ProfileItem from './ProfileItem';



const Profiles = ({ getProfiles, getCurrentProfile, profile: { profile, profiles, loading }, auth }) => {
  useEffect(() => {
    getProfiles();
    getCurrentProfile();
  }, [getProfiles, getCurrentProfile]);
 
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <h1 className='large text-primary'>New Friend Profiles </h1>
          <p className='lead'>
            <i className='fab fa-connectdevelop' /> Browse these profiles and
            find new friends!
          </p>
          <div className='profiles'>
            {profiles.length > 0 ? (
              profiles.map(singleprofile => ( auth.user._id !== singleprofile.user._id && 
                <ProfileItem key={singleprofile._id} singleprofile={singleprofile} />
              ))
            ) : (
              <h4> No Profiles Found... </h4>
            )}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Profiles.propTypes = {
  getProfiles: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { getProfiles, getCurrentProfile }
)(Profiles);
