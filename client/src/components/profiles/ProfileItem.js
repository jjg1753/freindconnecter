import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  sendfriendrequest,
  acceptfriendrequest,
  rejectfriendrequest,
  unfriend
} from '../../actions/friendship';

// Metod to find the mutual friends between current user and another user in the profile list
const mutualFriends = (profiles, user_id, friends) => {
  const user_profile = profiles.filter(e => e.user._id === user_id);
  const my_friends = user_profile[0].friends;

  var mutual = 0;

  for (var i = 0; i < my_friends.length; i++) {
    for (var j = 0; j < friends.length; j++) {
      if (my_friends[i]._id === friends[j]._id) {
        mutual = mutual + 1;
        break;
      }
    }

    //return profile.user._id;
    // return user_profile;
  }
  return mutual;
};

const onAccept = (from, to, acceptfriendrequest) => {
  acceptfriendrequest(from, to);
  window.location.reload(true);
};

const onUnfriend = (from, to, unfriend) => {
  unfriend(from, to);
  window.location.reload(true);
};

const onRejectfriendrequest = (from, to, rejectfriendrequest) => {
  rejectfriendrequest(from, to);
  window.location.reload(true);
};

const onSendfriendrequest = (from, to, sendfriendrequest) => {
  sendfriendrequest(from, to);
  window.location.reload(true);
};

const onWithdraw = (from, to, withdrawfriendrequest) => {
  withdrawfriendrequest(from, to);
  window.location.reload(true);
};

const ProfileItem = ({
  singleprofile: {
    user: { _id, name, avatar, summary, location, date_of_birth },
    company,
    interests,
    favoritefood,
    friends,
    receivedfriendrequests,
    sentfriendrequests
  },
  profile: { profiles },
  auth: { user },
  sendfriendrequest,
  acceptfriendrequest,
  rejectfriendrequest,
  unfriend
}) => {
  return (
    <Fragment>
      <div className='profile bg-light'>
        <img src={avatar} alt={name} className='round-ing' />
        <div>
          <h2>{name}</h2>
          <p>
            <i className='fas fa-briefcase' /> {company}
          </p>
          <p>
            <i className='fas fa-utensils' /> {favoritefood}
          </p>
          <p>
            {' '}
            <i className='fas fa-quote-right' /> {summary}
          </p>
          <p>
            {' '}
            <i class='fab fa-creative-commons' />{' '}
            {mutualFriends(profiles, user._id, friends)} Mutual Friends
          </p>
          <p className='my-1'>
            {location && (
              <span>
                <i className='fas fa-map-pin' /> {location}
              </span>
            )}
          </p>
          <Link to={`/profile/${_id}`} className='btn btn-primary'>
            View Full Profile
          </Link>
          <Fragment>
            {sentfriendrequests.some(e => e._id === user._id) ? (
              <Fragment>
                <button
                  onClick={() => onAccept(_id, user._id, acceptfriendrequest)}
                  className='btn btn-success'
                >
                  <i className='fas fa-user-plus' />{' '}
                  <span className='hide-sm'>Accept</span>{' '}
                </button>
                <button
                  onClick={() =>
                    onRejectfriendrequest(_id, user._id, rejectfriendrequest)
                  }
                  className='btn btn-danger'
                >
                  <i className='fas fa-user-minus' />{' '}
                  <span className='hide-sm'>Reject</span>{' '}
                </button>
              </Fragment>
            ) : (
              <Fragment>
                {receivedfriendrequests.some(e => e._id === user._id) ? (
                  <button
                    onClick={() =>
                      onWithdraw(user._id, _id, rejectfriendrequest)
                    }
                    className='btn btn-danger'
                  >
                    <i className='fas fa-user-minus' />{' '}
                    <span className='hide-sm'>Withdraw friend request</span>{' '}
                  </button>
                ) : (
                  <Fragment>
                    {friends.filter(e => e._id === user._id).length > 0 ? (
                      <Fragment>
                        <button
                          onClick={() => onUnfriend(user._id, _id, unfriend)}
                          className='btn btn-danger'
                        >
                          <i className='fas fa-user-minus' />{' '}
                          <span className='hide-sm'>Unfriend</span>{' '}
                        </button>
                      </Fragment>
                    ) : (
                      <button
                        onClick={() =>
                          onSendfriendrequest(user._id, _id, sendfriendrequest)
                        }
                        className='btn btn-primary'
                      >
                        <i className='fas fa-user-plus' />{' '}
                        <span className='hide-sm'>Add Friend</span>{' '}
                      </button>
                    )}{' '}
                  </Fragment>
                )}
              </Fragment>
            )}
          </Fragment>
        </div>
        <div>
          <h2>Interests</h2>
          <ul>
            {interests.slice(0, 4).map((interest, index) => (
              <li key={index} className='text-primary'>
                <i className='fas fa-check'> {interest}</i>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </Fragment>
  );
};

ProfileItem.propTypes = {
  singleprofile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  sendfriendrequest: PropTypes.func.isRequired,
  acceptfriendrequest: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  rejectfriendrequest: PropTypes.func.isRequired,
  unfriend: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { sendfriendrequest, acceptfriendrequest, rejectfriendrequest, unfriend }
)(ProfileItem);
