import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import FriendRequestItem from './FriendRequestItem'

const FriendRequests = ({ profile: { profile: {receivedfriendrequests}, loading } }) => {
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <h1 className='large text-primary'>New Friend Requests </h1>
          <p className='lead'>
            <i className='fab fa-connectdevelop' /> Connect with new friends by accepting these friend requests!
          </p>
          <div className='profiles'>
            {receivedfriendrequests.length > 0 ? (
              receivedfriendrequests.map(friend => (
                <FriendRequestItem key={friend._id} friend={friend} />
              ))
            ) : (
              <h4> No New Friend Requests Found... </h4>
            )}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

FriendRequests.propTypes = {
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { }
)(FriendRequests);
