import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {connect } from 'react-redux';
import { acceptfriendrequest, rejectfriendrequest } from '../../actions/friendship'

const FriendRequestItem = ({
  friend: {
    _id, name, avatar, summary, location
  }, auth: {user}, acceptfriendrequest, rejectfriendrequest
}) => {
  return (
    <div className='profile bg-light'>
      <img src={avatar} alt={name} className='round-ing' />
      <div>
        <h2>{name}</h2>
        <p>
          {' '}
          <i className='fas fa-quote-right' /> {summary}
        </p>
        <p className='my-1'>
          {location && (
            <span>
              <i className='fas fa-map-pin' /> {location}
            </span>
          )}
        </p>
        <Link to={`/profile/${_id}`} className='btn btn-primary'>
          View Full Profile
        </Link>
        <button
                  onClick={() => acceptfriendrequest(_id, user._id)}
                  className='btn btn-success'
                >
                  <i className='fas fa-user-plus' />{' '}
                  <span className='hide-sm'>Accept</span>{' '}
                </button>
                <button
                  onClick={() => rejectfriendrequest(_id, user._id)}
                  className='btn btn-danger'
                >
                  <i className='fas fa-user-minus' />{' '}
                  <span className='hide-sm'>Reject</span>{' '}
                </button>
      </div>
    </div>
  );
};

FriendRequestItem.propTypes = {
  auth: PropTypes.object.isRequired,
  acceptfriendrequest: PropTypes.func.isRequired,
  rejectfriendrequest: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, {acceptfriendrequest, rejectfriendrequest})(FriendRequestItem);
