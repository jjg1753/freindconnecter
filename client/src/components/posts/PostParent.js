import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPosts } from '../../actions/post';
import Spinner from '../layout/Spinner';
import PostItem from './PostItem';
import PostForm from './PostForm';
import { getCurrentProfile } from '../../actions/profile';

const PostParent = ({
  getCurrentProfile,
  getPosts,
  post: { posts },
  profile: { loading, profile }
}) => {
  useEffect(() => {
    getPosts();
    getCurrentProfile();
  }, [getPosts, getCurrentProfile]);
  return loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <h1 className='large text-primar'>Posts</h1>
      <p className='lead'>
        <i className='fas fa-user' />
        Welcome to the Community
      </p>
      <PostForm />
      <div className='posts'>
        {posts.map(
          post =>
            (profile.friends.filter(e => e._id === post.user).length > 0 ||
              profile.user._id === post.user) && (
              <PostItem key={post._id} post={post} />
            )
        )}
      </div>
    </Fragment>
  );
};

PostParent.propTypes = {
  getPosts: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  post: state.post,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getPosts, getCurrentProfile }
)(PostParent);
