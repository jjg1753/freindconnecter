import Axios from 'axios';

// This function will just take a token and if the token is validated then
// it will add it to the headers else it will delete it from the headers

const setAuthToken = token => {
  if (token) {
    Axios.defaults.headers.common['x-auth-token'] = token;
  } else {
    delete Axios.defaults.headers.common['x-auth-token'];
  }
};

export default setAuthToken;
