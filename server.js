// Config package is used to create global variables which can be accessed
// from anywhere in the application.

const express = require('express');
const app = express();
const connectToDb = require('./config/db');
const path = require('path');

connectToDb();

// Initializing Body Parser for parsing json reqs
app.use(
  express.json({
    extended: false
  })
);

// app.get('/', (req, res) => res.send('API Running'));

// Defining Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));

// Serve static assets in prod
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

// Either look for a set environment port variable or run on local port 5000
const PORT = process.env.PORT || 5000;

app.listen(PORT, () =>
  console.log(`Server successfully started running on Port ${PORT}`)
);
