const mongoose = require('mongoose');

// Config package is used to create global variables which can be accessed
// from anywhere in the application.
const config = require('config');

const db = config.get('mongoURL');

/* Not using .then .catch syntax, instead using sync await 
to make the code more synchronous in functionality even though 
it is still asynchronous */

//Always use a try catch block with sync await function 

const connectToDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false
        });

        console.log('Connection to Mongo Database Established Successfully...');
    } catch (err) {
        console.error(err.message);

        // Graceful exit of the process in case of a failure in connection
        process.exit(1);
    }
};

module.exports = connectToDB;